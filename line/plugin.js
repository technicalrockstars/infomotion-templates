var d3 = window.d3;
var Rainbow = require('rainbowvis.js');

var rainbow = new Rainbow();
rainbow.setSpectrum('FFFBCA', 'F5CE83', 'EEA755', 'E77E2F', 'D5370B');
rainbow.setNumberRange(0, 100);

LineChart.defaultSettings = {
    "label" : "lang",
    "value": "uu",
    "labelNames" : []
};

LineChart.settings = EnebularIntelligence.SchemaProcessor([
{
  type : "key", name : "label", help : "ラベルとなるデータのkeyを指定してください。"
},{
  type : "key", name : "value", help : "値を表すデータのkeyを指定してください。"
},{
  type : "list", name : "labelNames", help : "ラベルの表示名を設定します（オプション）", children : [
    { type: "text", name : "key" },
    { type : "text", name : "value" }
  ]}
], LineChart.defaultSettings);



function LineChart(settings, options) {
    var that = this;
    this.el = window.document.createElement('div');
    this.settings = settings;
    this.options = options;
    this.data = [];

    var margin = {top: 50, right: 120, bottom: 50, left: 120},
        width = (options.width || 700) - margin.left - margin.right,
        height = (options.height || 500) - margin.top - margin.bottom;

    this.width = width;
    this.height = height;
    this.margin = margin;

    this.x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

    this.yLine = d3.scale.linear()
      .range([height, 0]);

    this.xAxis = d3.svg.axis()
      .scale(this.x)
      .orient("bottom");

    this.yLineAxis = d3.svg.axis()
      .scale(this.yLine)
      .orient("left");

    this.line = d3.svg.line()
                .x(function(d) {
                    return that.x(d.key);
                })
                .y(function(d) {
                    return that.yLine(d.value);
                })
                .interpolate("cardinal")
                .tension(0.9);

    this.svg = d3.select(this.el).append("svg")
                  .attr('class', 'linechart')
                  .attr('class', 'svgWrapper')
                  .attr("width", width + margin.left + margin.right)
                  .attr("height", height + margin.top + margin.bottom);

    this.base = this.svg.append("g")
      .attr("transform", "scale(1,1)translate(" + margin.left + "," + margin.top + ")");

    this.barBase = this.base.append("g");

    this.base.append("g")
        .attr("class", "y linechart__axis")
        .call(that.yLineAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".3em")
        .style("text-anchor", "end")
        .style("fill", "#17718A")
        .text(that.settings.value);

    this.base.append("g")
        .attr("class", "x linechart__axis")
        .attr("transform", "translate(0," + that.height + ")")
        .call(that.xAxis)
        .append("text")
        .attr("transform", "rotate(0)")
        .attr("x", width + margin.left)
        .attr("y", 12)
        .attr("dy", ".71em")
        .style("text-anchor", "end");



    this.path = this.base.append("path") // Add the line path.
        .attr("class", "linechart__line");

    // this.point = this.base
    //             .append("g")
    //             .attr("class", "barchart__linePoint");
}

LineChart.prototype.onDrillDown = function(cb) {
    this.onDrillDownListener = cb;
}

LineChart.prototype.addData = function(data) {
    var that = this;
    if(data instanceof Array) {
        //合計
        data.forEach(function(d) {
            that.data.push(d);
        });
        this.refresh();
    }else{
        //瞬間
        this.data = [];
        this.data.push(data);
        this.refresh();
    }
}

LineChart.prototype.clearData = function() {
    this.data = [];
    this.refresh( );
}

LineChart.prototype.calculate = function() {
    var that = this;
    var newdataLine = {};
    this.data.forEach(function(d) {
        var k = d[that.settings.label];
        if(!newdataLine[k]) newdataLine[k] = 0;
        newdataLine[k] += d[that.settings.value];
    });
    return Object.keys(newdataLine).map(function(k) {
        return {
            key : k,
            value : newdataLine[k],
        }
    });
}

LineChart.prototype.resize = function(options) {
    var that = this;
    // var ww = options.width / this.options.width;
    // var hh = options.height / this.options.height;
    // var ss = ww < hh ? ww : hh;

    this.height = options.height - that.margin.top - that.margin.bottom;
    this.width = options.width - that.margin.left - that.margin.right;

    this.x.rangeRoundBands([0, this.width], .1);
    this.yLine.range([this.height, 0]);

    this.refresh();

}

LineChart.prototype.refresh = function() {
    var that = this;
    var data = this.calculate();

    this.x.domain(data.map(function(d) { return d.key; }));
    this.yLine.domain([0, d3.max(data, function(d) { return d.value; })]);

    this.path
    .datum(data)
    .transition()
    .duration(500)
    .attr("d", that.line(data))
    .attr("transform", "translate(" + that.x.rangeBand()/2 + ", 0)");

    var point = this.base.selectAll('circle').data(data);
    point.enter().append('circle');

    point.attr("r", 5)
        .style("fill", '#17718A')
        .attr("transform", "translate(" + that.x.rangeBand()/2 + ", 0)")
        .transition()
        .duration(500)
        .attr("cx", function(d, i) {
            return that.x(d.key);
        })
        .attr("cy", function(d, i) { return that.yLine(d.value) });

    point.exit().remove();


    this.svg = d3.select(this.el).transition();

    this.svg.select(".x.linechart__axis")
        .duration(500)
        .attr("transform", "translate(0," + that.height + ")")
        .call(this.xAxis);

    this.svg.select(".y.linechart__axis")
        .duration(500)
        .call(this.yLineAxis);

}

function percentToColor(num) {
    return {
        background: '' +rainbow.colourAt(num),
        text: (num>77) ? 'fff' : '222'
    }
}

function addComma(v) {
    var s = String(v);
    var r = '';
    for(var i=s.length-1;i >= 0;i--) {
        var j = s.length - i - 1;
        if( j != 0 && j % 3 == 0 ) r = ',' + r;
        r = s[i] + r
    }
    return r;
}

LineChart.prototype.getEl = function() {
  return this.el;
}

window.EnebularIntelligence.register('linechart', LineChart);
module.exports = LineChart; 
