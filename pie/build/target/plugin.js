(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var d3 = window.d3;

var color_list = [
// purple
"#98abc5",
"#8a89a6",
"#7b6888",
"#6b486b",
"#a05d56",
"#d0743c",
"#ff8c00"
];


var color_list = [
// orange
"#D23001",
"#D64416",
"#DA582B",
"#DF6C40",
"#E38055",
"#E8956B",
"#ECA980",
"#F0BD95",
"#F5D1AA",
"#F9E5BF",
"#FEFAD5"
];

/*
Settings
*/

PieChart.defaultSettings = {
	"label" : "node_id",
	"value" : "ss",
	"labelNames" : [
		{ '0000001' : 'SNS' },
		{ '0000002' : 'Search Engine' },
		{ '0000003' : 'Other Web Site' },
		{ '0000004' : 'Official Web Site' },
		{ '0000005' : 'Online Store' }
	]
};

PieChart.settings = EnebularIntelligence.SchemaProcessor([
{
  type : "key", name : "label", help : "ラベルとなるデータのkeyを指定してください。"
},{
  type : "key", name : "value", help : "値を表すデータのkeyを指定してください。"
},{
  type : "list", name : "labelNames", help : "ラベルの表示名を設定します（オプション）", children : [
    { type: "text", name : "key" },
    { type : "text", name : "value" }
  ]}
]);

function PieChart(settings, options) {
	var that = this;
	this.el = window.document.createElement('div');
	var width = options.width || 680;
	var height = options.height || 450;
	that.radius = Math.min(width, height) / 2 - 40;

	pieChartWidth = 585;
	pieChartHeight = 295;

	this.scaleRatio = 1;

	this.options = options;

	this.svg = d3.select(this.el)
		.append("svg")
		.attr('class', 'svgWrapper');


	this.base = this.svg
		.attr('width', width)
		.attr('height', height)
		.append("g")
		.attr('class', 'piechart');

	this.base.append("g")
		.attr("class", "piechart__lines");
	this.base.append("g")
		.attr("class", "piechart__slices");
	this.base.append("g")
		.attr("class", "piechart__labels");

	this.pie = d3.layout.pie()
		.sort(null)
		.value(function(d) {
			return that.getValue(d);
		});

	this.arc = d3.svg.arc()
		.outerRadius(that.radius * 0.8)
		.innerRadius(that.radius * 0.4);

	this.outerArc = d3.svg.arc()
		.innerRadius(that.radius * 0.9)
		.outerRadius(that.radius * 0.9);

	this.base.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	this.data = [];

	this.setLabels(settings);
	this.setSchema(settings);
}

PieChart.prototype.onDrillDown = function(cb) {

}

PieChart.prototype.addData = function(data) {
    var that = this;
    if(data instanceof Array) {
        data.forEach(function(d) {
            that.data.push(d);
        });
        this.refresh();
    }else{
        this.data.push(data);
        this.refresh();
    }
}

PieChart.prototype.clearData = function() {
	this.data = [];
	this.refresh();
}

PieChart.prototype.calculate = function() {
	var that = this;
	var newdata = {};
	this.data.forEach(function(d) {
		var k = d[that.schema.label];
		if(!newdata[k]) newdata[k] = 0;
		newdata[k] += d[that.schema.value];
	});
	return Object.keys(newdata).map(function(k) {
		return {
			key : k,
			value : newdata[k]
		}
	});
}


PieChart.prototype.setSchema = function(schema) {
	this.schema = schema;
}

PieChart.prototype.setLabels = function(labels) {
	this.labels = labels;
	var names = Object.keys(this.labels.labelNames);
	//.map(function(k) {return labels.LabelNames[k];});
	if(names.length == 0) {
		var colors = color_list;
	}else{
		var colors = color_list.slice(0, names.length);
	}
	this.color = d3.scale.ordinal()
		.domain(names)
		.range(colors);
}

PieChart.prototype.getLabel = function(d) {
	return this.labels.labelNames[d.data.key] ? this.labels.labelNames[d.data.key] : d.data.key;
}

PieChart.prototype.getValue = function(d) {
	return d.value;
}

PieChart.prototype.refresh = function() {
	this.change( this.calculate() );
}

PieChart.prototype.resize = function(options) {

	this.radius = Math.min(options.width, options.height) / 2 - 40;

	var margin = {
		top: 30,
		left: 30,
		bottom: 30,
		right: 30
	};

	var xRatio = (options.width - margin.left - margin.right) / pieChartWidth;
	var yRatio = (options.height - margin.top - margin.bottom) / pieChartHeight;

	this.scaleRatio = (xRatio > yRatio) ? yRatio : xRatio;
	if(this.scaleRatio > 1) this.scaleRatio = 1;

	var translate = {
		x: options.width/2/this.scaleRatio ,
		y: options.height/2/this.scaleRatio
	}

	this.arc = d3.svg.arc()
    .outerRadius(this.radius * 0.8)
    .innerRadius(this.radius * 0.4);

  this.outerArc = d3.svg.arc()
    .innerRadius(this.radius * 0.9)
    .outerRadius(this.radius * 0.9);

	this.base
	.attr("width", options.width)
	.attr("height", options.height)
	.transition().duration(500)
	.attr("transform", "scale(" + this.scaleRatio + "," + this.scaleRatio + ") translate(" + translate.x + "," + translate.y + ")");
}

PieChart.prototype.change = function(data) {
	var that = this;
	var key = function(d){ return that.getLabel(d); };
	/* ------- PIE SLICES -------*/
	var slice = this.base.select(".piechart__slices").selectAll("path.piechart__slice")
		.data(this.pie(data), key);

	slice.enter()
		.insert("path")
		.style("fill", function(d) { return that.color(d.data.key); })
		.attr("class", "piechart__slice");

	slice
		.transition().duration(1000)
		.attrTween("d", function(d) {
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				return that.arc(interpolate(t));
			};
		})

	slice.exit()
		.remove();

	/* ------- TEXT LABELS -------*/

	var text = this.base.select(".piechart__labels").selectAll("text")
		.data(that.pie(data), key);

	text.enter()
		.append("text")
		.attr("dy", ".35em")
		.text(function(d) {
			return that.getLabel(d);
		});

	function midAngle(d){
		return d.startAngle + (d.endAngle - d.startAngle)/2;
	}

	text.transition().duration(1000)
		.attrTween("transform", function(d) {
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				var pos = that.outerArc.centroid(d2);
				pos[0] = that.radius * (midAngle(d2) < Math.PI ? 1 : -1);
				return "translate("+ pos +")";
			};
		})
		.styleTween("text-anchor", function(d){
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				return midAngle(d2) < Math.PI ? "start":"end";
			};
		});

	text.exit()
		.remove();

	/* ------- SLICE TO TEXT POLYLINES -------*/

	var polyline = this.base.select(".piechart__lines").selectAll("polyline")
		.data(this.pie(data), key);

	polyline.enter()
		.append("polyline");

	polyline.transition().duration(1000)
		.attrTween("points", function(d){
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				var pos = that.outerArc.centroid(d2);
				pos[0] = that.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
				return [that.arc.centroid(d2), that.outerArc.centroid(d2), pos];
			};
		});

	polyline.exit()
		.remove();
};


PieChart.prototype.getEl = function() {
	return this.el;
}

PieChart.prototype.createDialog = function() {
	return {
		properties : [{
			name : 'value',
			type : 'property'
		}]
	}
}

window.EnebularIntelligence.register('piechart', PieChart);

module.exports = PieChart;
},{}]},{},[1]);
